!function ($) {
    "use strict"; // jshint ;_;
    var GridPagerInit = function (element, options) {
        this.init(element, options);
    };
    var c = function(r){console.log(r)};
    GridPagerInit.prototype = {
        constructor: GridPagerInit
        , init: function (element, options) {
            var GPI = this
                , $el = $(element)
                , currNavs = [];
            GPI.$el = $el;
            GPI.$options  = GPI.getOptions(options);
            GPI.$cols     = $el.find('.'+GPI.$options.cls);//reference to all classname els found within container
            GPI.$numItems = GPI.$cols.length;//number of classname els found within container
            GPI.$pgmin    = 1;
            GPI.$pgNum = 1;
            GPI.$pgmax    = Math.ceil(GPI.$numItems/GPI.$options.tpp);
            GPI.$currBtn = $();
            GPI.$currNavs = currNavs;
            GPI.$elli     = "...";
            GPI.setStyles();
            //GPI.changePage('#'+GPI.$options.navid+' li a');
            if(GPI.shouldPaginate() == true){
                GPI.initNavs();
                GPI.initPage(GPI.$currBtn);
                GPI.listen();
            }else{
                GPI.initPage();
            }
            //c(GPI.shouldPaginate());
        }
        , shouldPaginate: function(){var GPI = this;if(GPI.$numItems > GPI.$options.tpp){return true;}else{return false;}}
        , initNavs: function(){//_________________SETUP THE CONTROLS_________________
            var GPI = this, j, navul
                , controls_html = '<nav id="'+GPI.$options.navid+'"><ul class="gpagination">';

            for(j=1;j<=GPI.$pgmax;j++){//______MAKE UP TO 8 BUTTONS, STOP IF PGMAX REACHED
                if(j<9) {
                    controls_html += '<li><a class="pagbtn" href="javascript:void(0);" '
                        +'data-go-to="'+j+'" id="btn'+j+'">' + (j) + '</a></li>';
                }
            }
            if(GPI.$pgmax>=9){//if PGMAX 9 or greater, make a 9th BUTTON WITH PGMAX VALUE
                controls_html += '<li><a class="pagbtn" href="javascript:void(0);" '
                    +'data-go-to="'+GPI.$pgmax+'" id="btn9">'+GPI.$pgmax+'</a></li>';
            }

            controls_html += '</ul></nav>';
            GPI.$el.after(controls_html);
            GPI.$currNavs = $('#'+GPI.$options.navid+'>ul li>a');//set $currNavs
            navul = $('#'+GPI.$options.navid+' ul.gpagination');
            GPI.$currBtn = $(":first-child>a", navul);//pass $currBtn to initPage
            GPI.sel(1);
        }
        , initPage: function (e){//_______SHOW GRID ITEMS eq0-3
            var GPI = this, i;
            GPI.$pgNum = 1;
            for(i=0;i<GPI.$options.tpp;i++){//tpp is max possible to show at first
                if (i<GPI.$numItems){//don't loop unnecessarily - stop if $numItems reached
                    GPI.$el.find('.'+GPI.$options.cls+':eq('+i+')').removeClass("hide").addClass("show");
                }
            }
        }
        , listen: function(){
            var GPI = this;
            //"pager" is $("grid-pager-nav li a") elmts by default (all a elmts in li elmts in container)
            var pager = $('#'+GPI.$options.navid+' li a');
            //onClick call changePage with the 'a' elmt as the "e" argument
            pager.on('click',  $.proxy(GPI.changePage, GPI));
        }
        , changePage: function (e){
            //_______CALLED BY: LISTEN - TAKES PG NUM ARG [DATA-GO-TO VAL]
            //_______AND CALL UPDATENAVS PASSING CHOSEN [DATA-GO-TO VAL],
            var GPI = this, startshow, endshow, i;

            GPI.$pgNum = $(e.currentTarget).data('go-to');      //__SET $pgNum BASED ON CLICK
            c("----<>changePage, datagoto is: "+GPI.$pgNum);
            startshow = (GPI.$pgNum-1)*GPI.$options.tpp;        //__DETERMINE SET TO SHOW 1:1  2:4  3:8  4:12
            endshow = (startshow+GPI.$options.tpp);             //__                  END 5
            GPI.$el.find('.'+GPI.$options.cls).removeClass("show").addClass("hide");              //__HIDE ALL ITEMS
            for(i=startshow;i<endshow;i++){                     //__SHOW DETERMINED SET
                    GPI.$el.find('.'+GPI.$options.cls+':eq('+i+')').removeClass("hide").addClass("show");
            }
            if(GPI.$pgmax>9){//SHOULD NAVS BE DYNAMIC??? THEN...
                GPI.updateNavs(GPI.$pgNum);//_______CALL UPDATENAVS WITH $pgNum (from GOTO VAL)
            }else{
                GPI.updateFewerNavs(GPI.$pgNum);
            }
        }
        , updateFewerNavs: function(p){
            var GPI = this, i;
            GPI.$currNavs.eq(7).text(GPI.$elli);//b8 ellipsis, 1-7 low num range
            for(i=1;i<=GPI.$pgmax;i++){               //count up from min
                GPI.$currNavs.eq(i-1).text(i); //set text within 9btns
                GPI.$currNavs.eq(i-1).attr( 'data-go-to', i );
            }
            $(".pagbtn").removeClass('gpaghoversel').addClass('gpaghover');         //all
            $("#btn"+p ).removeClass('gpaghover').addClass('gpaghoversel');         //selected

        }
        , updateNavs: function(p){//______CHANGE THE CONTROLS - CALLS SEL AND NUMS
            var GPI = this, m = GPI.$pgmax;
                 if ( p == 1            ){ GPI.nums_bot(); GPI.sel(1); }
            else if ( p == 2            ){ GPI.nums_bot(); GPI.sel(2); }
            else if ( p == 3            ){ GPI.nums_bot(); GPI.sel(3); }
            else if ( p == 4            ){ GPI.nums_bot(); GPI.sel(4); }
            else if ( p == 5            ){ GPI.nums_bot(); GPI.sel(5); }
            else if ( p  > 5 && p < m-4 ){ GPI.nums_mid(p);GPI.sel(5); }
            else if ( p == m-4          ){ GPI.nums_top(); GPI.sel(5); }
            else if ( p == m-3          ){ GPI.nums_top(); GPI.sel(6); }
            else if ( p == m-2          ){ GPI.nums_top(); GPI.sel(7); }
            else if ( p == m-1          ){ GPI.nums_top(); GPI.sel(8); }
            else if ( p == m            ){ GPI.nums_top(); GPI.sel(9); }
            GPI.$currNavs = $('#'+GPI.$options.navid+'>ul li>a');//update currNavs with a elmnts
        }
        , sel: function(b){//______INDICATE CURRENT BUTTON_________________
            $(".pagbtn").removeClass('gpaghoversel').addClass('gpaghover');         //all
            $("#btn"+b ).removeClass('gpaghover').addClass('gpaghoversel');         //selected
        }
        , nums_bot: function(){//_______REASSIGN BUTTONS TEXT AND LINKS (BOTTOM)_________________
            var GPI = this, navs = GPI.$currNavs, i;
            c('nums_bot() called');
            //c('navs are');c(navs);c('yup');
            $(navs[0]).text(GPI.$pgmin   ).data('goTo', GPI.$pgmin   );/*both ellipsis, dynamic nums*/
            $(navs[1]).text(GPI.$pgmin+1 ).data('goTo', GPI.$pgmin+1 );/*both ellipsis, dynamic nums*/
            $(navs[2]).text(GPI.$pgmin+2 ).data('goTo', GPI.$pgmin+2 );
            $(navs[3]).text(GPI.$pgmin+3 ).data('goTo', GPI.$pgmin+3 );
            $(navs[4]).text(GPI.$pgmin+4 ).data('goTo', GPI.$pgmin+4 );
            $(navs[5]).text(GPI.$pgmin+5 ).data('goTo', GPI.$pgmin+5 );
            $(navs[6]).text(GPI.$pgmin+6 ).data('goTo', GPI.$pgmin+6 );
            $(navs[7]).text(GPI.$pgmin+7 ).data('goTo', GPI.$pgmin+7 );
            $(navs[8]).text(GPI.$pgmax   ).data('goTo', GPI.$pgmax   );
        }
        , nums_mid: function(curpg){//_______REASSIGN BUTTONS TEXT AND LINKS (MID)_________________
            var GPI = this, navs = GPI.$currNavs, i, j;
            c('nums_mid() called');c(curpg);
            //c('navs are');c(navs);c('yup');
            $(navs[0]).text(GPI.$pgmin).data('goTo', GPI.$pgmin );
            $(navs[1]).text(curpg-3   ).data('goTo', (curpg-3)  );//both ellipsis
            $(navs[2]).text(curpg-2   ).data('goTo', (curpg-2)  );//dynamic nums
            $(navs[3]).text(curpg-1   ).data('goTo', (curpg-1)  );
            $(navs[4]).text(curpg     ).data('goTo', (curpg  )  );
            $(navs[5]).text(curpg+1   ).data('goTo', (curpg+1)  );
            $(navs[6]).text(curpg+2   ).data('goTo', (curpg+2)  );
            $(navs[7]).text(curpg+3   ).data('goTo', (curpg+3)  );
            $(navs[8]).text(GPI.$pgmax).data('goTo', GPI.$pgmax );
            //c('navs are');c(navs);c('yup');
        }
        , nums_top: function(){//_______REASSIGN BUTTONS TEXT AND LINKS (TOP)_________________
            var GPI = this, navs = GPI.$currNavs, i;
            c('nums_top() called');
            $(navs[0]).text(GPI.$pgmin   ).data('goTo', GPI.$pgmin   );
            $(navs[1]).text(GPI.$pgmax-7 ).data('goTo', GPI.$pgmax-7 );//#2 ellipsis
            $(navs[2]).text(GPI.$pgmax-6 ).data('goTo', GPI.$pgmax-6 );
            $(navs[3]).text(GPI.$pgmax-5 ).data('goTo', GPI.$pgmax-5 );//high nums
            $(navs[4]).text(GPI.$pgmax-4 ).data('goTo', GPI.$pgmax-4 );
            $(navs[5]).text(GPI.$pgmax-3 ).data('goTo', GPI.$pgmax-3 );
            $(navs[6]).text(GPI.$pgmax-2 ).data('goTo', GPI.$pgmax-2 );
            $(navs[7]).text(GPI.$pgmax-1 ).data('goTo', GPI.$pgmax-1 );
            $(navs[8]).text(GPI.$pgmax   ).data('goTo', GPI.$pgmax   );
        }

        , getOptions: function (options) {
            var GPI = this;
            options = $.extend({}, $.fn['gridPager'].defaults, options, GPI.$el.data());
            return options;
        }
        , setStyles: function(){
            var GPI=this, gpcss, fig_fontSize, fig_padtop;

            fig_fontSize =  (GPI.$options.navsize/2.7)+"px";
            fig_padtop =    (GPI.$options.navsize/4.5)+"px";

            gpcss = "\
            ."+GPI.$options.cls+"{\
                display: none;\
            }\
            #"+GPI.$options.navid+"{\
                text-align:center;\
                font-size:"+fig_fontSize+";\
            }\
            #grid-pager {\
                height:"+GPI.$options.gridsize+"px;\
            }\
            .gpagination {\
                display: inline-block;\
                padding-left: 0;\
                margin: "+GPI.$options.navmargin+"px 0;\
                border-radius: "+GPI.$options.corners+"px;\
            }\
            .gpagination>li {\
                display: inline;\
            }\
            .gpagination>li:first-child>a, \
            .gpagination>li:first-child>span {\
                margin-left: 0;\
                border-bottom-left-radius: "+GPI.$options.corners+"px;\
                border-top-left-radius: "+GPI.$options.corners+"px;\
            }\
            .gpagination>li:last-child>a, \
            .gpagination>li:last-child>span {\
                border-bottom-right-radius: "+GPI.$options.corners+"px;\
                border-top-right-radius: "+GPI.$options.corners+"px;\
            }\
            .gpaghoversel {\
                background-color: black;\
            }\
            .gpaghover {\
                background-color: blue;\
            }\
            .gpaghover:hover {\
                background-color: #000055;\
            }\
            .gpaghoversel:hover {\
                background-color: #111111;\
            }\
            .gpagination>li>a{\
                //display: table-cell;\
                position: relative;\
                float: left;\
                text-align: center;\
                line-height: 1.42857143;\
                text-decoration: none;\
                color: #ffffff;\
                border: 1px solid transparent;\
                margin-left: -1px;\
                padding-top: "+fig_padtop+";\
                width: "+(GPI.$options.navsize+10)+"px;\
                height: "+GPI.$options.navsize+"px;";
            if(GPI.$options.font){
                gpcss += "font-family:"+GPI.$options.fontfam+";";
            }else{
                gpcss += "";
            }
            gpcss += "}";
            var tempDiv = document.createElement('div');tempDiv.innerHTML = '<p>Odoyle Rules</p><style>' + gpcss + '</style>';
            document.getElementsByTagName('head')[0].appendChild(tempDiv.childNodes[1]);

        }

    };//END:GridPagerInit.prototype

    /* GridPagerInit PLUGIN DEFINITION
     * ======================== */
    $.fn.gridPager = function (option) {
        return this.each(function () {
            var $this = $(this)
                , data = $this.data('gridPager')
                , options = typeof option == 'object' && option;
            if (!data) $this.data('gridPager', (data = new GridPagerInit(this, options)));
            if (typeof option == 'string') data[option]()
        })
    };

    $.fn.gridPager.Constructor = GridPagerInit;
    /* GridPagerInit DATA-API
     * =============== */
    $.fn.gridPager.defaults = {
        cls: '.grid-pager',
        tpp: 12,
        navid: 'gpnav',
        navsize: '40',
        corners: 5,
        fontfam: "sans-serif"
    }

}(window.jQuery);