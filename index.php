<!DOCTYPE html><html lang="en"><head><meta charset="utf-8"><title>VI-gridpager</title>
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <link href="src/bootstrap/css/bootstrap.darkly.min.css" rel="stylesheet">
    <link href="src/css/styles.css" rel="stylesheet">
</head>

<body>
<div class="container-fluid">
    <div class="row"><div class="text-center panel"><span style="font-size:32px;">Grid-Pager</span></div></div>

    <div id="grid-pager" class="row">
        <?php
        $html = '';
        for ($i = 1; $i <= 1900; $i++) {
            $html .= '<div class="col-md-2 hide grid-pager">';
            $html .=    '<div class="panel panel-default panel-kiosk" ';$html.='style="background-image:url(';$html.="'src/img/placeholder.png'";$html.=');">';
            $html .=        '<div class="panel-heading">';
            $html .=            '<h3 class="panel-title">image'.$i.'</h3>';
            $html .=        '</div>';
            $html .=        '<div class="panel-body">';
            $html .=            '<div class="panel-btns">';
            $html .=                '<button type="button" class="btn btn-xs btn-block">';
            $html .=                    '<em class="glyphicon glyphicon-remove-circle"></em> Default';
            $html .=                '</button>';
            $html .=            '</div>';
            $html .=        '</div>';
            $html .=    '</div>';
            $html .= '</div>';
        }
        echo $html;
        ?>

    </div>
</div>

<!-- ==========JS=========== -->
<!--##JQUERY-->
<script src="src/js/jquery-1.11.3.min.js" type="text/javascript"></script>
<!--##BOOTSTRAP_3.3-->
<script src="src/bootstrap/js/bootstrap.min.js"></script>
<!--##USER(JS)-->
<script src="src/js/grid-pager.js"></script>

<script>

    $('#grid-pager').gridPager({
        cls: 'grid-pager', // the columns within the row
        navid: 'gpnav',         // total per page
        tpp: 6,             // total per page
        gridsize: 120,
        navmargin: 10,
        navsize: 70,
        corners: 15,
//        fontfam: "Courier, \"Lucida Console\", monospace",
//        fontfam: "\"Arial\"",
//        fontfam: "sans-serif", //default
//        fontfam: '',   //inherit
        fontfam: ''
    });

</script>

</body>
</html>